package com.randomlake.moviesvc.service;

import com.randomlake.moviesvc.model.Movie;
import com.randomlake.moviesvc.repository.MovieRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

// The service class is where most of the business logic goes.
// It uses the repository, which talks to the database, to get the movies and return the data.
@Service
public class MovieService {
    @Autowired
    private MovieRepository movieRepository;

    public List<Movie> allMovies() {
        return movieRepository.findAll();
    }

    public Optional<Movie> singleMovie(String imdbId) {
        return movieRepository.findMovieByImdbId(imdbId);
    }

}
