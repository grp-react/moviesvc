package com.randomlake.moviesvc.repository;

import com.randomlake.moviesvc.model.Movie;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

// The repository is the data access layer. It does the job of
// talking to the database and getting the data back.
@Repository
public interface MovieRepository extends MongoRepository<Movie, ObjectId> {

    // We don't want to expose the MongoDb Object Id for searching for various reasons
    // We can enable finding a moving by the imdbId this way:
    Optional<Movie> findMovieByImdbId(String imdbId);

}
