package com.randomlake.moviesvc.controller;

import com.randomlake.moviesvc.model.Movie;
import com.randomlake.moviesvc.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

// The controller is the API layer. It's only concern is getting a request and returning a response.
// It needs a service class to do this, but it doesn't know what's going on inside the service class.
@RestController
@RequestMapping("/api/v1/movies")
@CrossOrigin(origins = "*")
public class MovieController {

    @Autowired
    private MovieService movieService;

    // Here it calls 'allMovies' in the service layer class to get the data and return a list.
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        return new ResponseEntity<List<Movie>>(movieService.allMovies(), HttpStatus.OK);
    }

    // Here it calls singleMove in the service layer class to get the data for a single movie.
    @GetMapping("/{imdbId}")
    public ResponseEntity<Optional<Movie>> getSingleMovie(@PathVariable String imdbId) {
        return new ResponseEntity<Optional<Movie>>(movieService.singleMovie(imdbId), HttpStatus.OK);
    }

}
